package com.boa.qa;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.HttpMethod;

import org.testng.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class CreateJiraBug {

	private DataFromPropertyFile fromPropertyFile;

	public void connect(String url, String userName, String password,
			String methodType, String fileNameToSave) {
		
		StringBuilder stringBuilder;

		String authStr = userName + ":" + password;
		try {

			URL jiraREST_URL = new URL(url);
			// URL jiraREST_URL = new
			// URL("http://localhost:8086/rest/api/2/issue/");
			URLConnection urlConnection = jiraREST_URL.openConnection();
			urlConnection.setDoInput(true);

			HttpURLConnection conn = (HttpURLConnection) jiraREST_URL
					.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);

			conn.setRequestMethod(methodType);

			// conn.setRequestProperty("Authorization", "Basic " +
			// Base64.encode("mohsinazaman:mohsina1jira".getBytes(),0));
			conn.setRequestProperty("Authorization",
					"Basic " + Base64.encode(authStr.getBytes(), 0));

			conn.setRequestProperty("Content-Type", "application/json");

			conn.getOutputStream().write(getJSON_Body().getBytes());

			try {

				if (conn.getResponseCode() == 201) {

					stringBuilder = new StringBuilder();
					System.out.println("printing status Code: "
							+ conn.getResponseCode());

					Assert.assertEquals(conn.getResponseCode(), 201);
					BufferedReader inputStream = new BufferedReader(
							new InputStreamReader(conn.getInputStream(),
									Charset.forName("UTF-8")));

					String line = null;
					while ((line = inputStream.readLine()) != null) {
						System.out.println(line);
						stringBuilder.append(line + "\n");
					}
					// System.out.println(toPrettyFormat(stringBuilder.toString()));

					saveJsonResponseToFile(fileNameToSave,
							toPrettyFormat(stringBuilder.toString()));

				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void connect() {
		String path_Response_File = "C:\\Users\\PaxoTech Student\\Desktop\\JsonResponse\\response_1";
		String path_ResponseParse_File = "C:\\Users\\PaxoTech Student\\Desktop\\JsonResponse\\response_1.json";
		String path = System.getProperty("user.dir");
		fromPropertyFile = new DataFromPropertyFile(path
				+ "\\src\\test\\resources\\Resources\\PropertyFile.property");
		String url = fromPropertyFile.readFromPropertyFile("server_name");
		String basePath = fromPropertyFile.readFromPropertyFile("base_path");
		String action = fromPropertyFile.readFromPropertyFile("action");
		String userName = fromPropertyFile.readFromPropertyFile("user_name");
		String password = fromPropertyFile.readFromPropertyFile("password");
		String authStr = userName + ":" + password;
		StringBuilder stringBuilder;
		JsonResponseParse responseParse;
		try {

			URL jiraREST_URL = new URL(url + basePath + action);
			// URL jiraREST_URL = new
			// URL("http://localhost:8086/rest/api/2/issue/");
			URLConnection urlConnection = jiraREST_URL.openConnection();
			urlConnection.setDoInput(true);

			HttpURLConnection conn = (HttpURLConnection) jiraREST_URL
					.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);

			conn.setRequestMethod(HttpMethod.POST);

			// conn.setRequestProperty("Authorization", "Basic " +
			// Base64.encode("mohsinazaman:mohsina1jira".getBytes(),0));
			conn.setRequestProperty("Authorization",
					"Basic " + Base64.encode(authStr.getBytes(), 0));

			conn.setRequestProperty("Content-Type", "application/json");

			conn.getOutputStream().write(getJSON_Body().getBytes());

			try {

				if (conn.getResponseCode() == 201) {

					stringBuilder = new StringBuilder();
					System.out.println("printing status Code: "
							+ conn.getResponseCode());

					Assert.assertEquals(conn.getResponseCode(), 201);
					BufferedReader inputStream = new BufferedReader(
							new InputStreamReader(conn.getInputStream(),
									Charset.forName("UTF-8")));

					String line = null;
					while ((line = inputStream.readLine()) != null) {
						System.out.println(line);
						stringBuilder.append(line + "\n");
					}
					// System.out.println(toPrettyFormat(stringBuilder.toString()));

					saveJsonResponseToFile(path_Response_File,
							toPrettyFormat(stringBuilder.toString()));
					responseParse = new JsonResponseParse(
							path_ResponseParse_File);

					responseParse.getElementByName("id");
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static String getJSON_Body() {
		JsonObject createIssue = Json
				.createObjectBuilder()
				.add("fields",
						Json.createObjectBuilder()
								.add("project",
										Json.createObjectBuilder().add("key",
												"MER"))
								.add("summary", "summery")
								.add("description", "description")
								.add("issuetype",
										Json.createObjectBuilder().add("name",
												"Bug"))).build();

		return createIssue.toString();

	}

	public String toPrettyFormat(String jsonString) {
		String prettyJson = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			Object jsonObject = mapper.readValue(jsonString, Object.class);
			prettyJson = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(jsonObject);
			System.out.println(prettyJson);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prettyJson;
	}

	public void saveJsonResponseToFile(String fileLocation, String jsonString) {
		try {
			FileWriter file = new FileWriter(fileLocation + ".json");
			file.write(jsonString);
			System.out.println("Successfully add json object to file...!!");
			file.close();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}

}
