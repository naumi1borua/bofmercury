package com.boa.qa;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

public class DataFromPropertyFile {

	private static File file;
	private static FileInputStream fileInput;
	private static OutputStream out;
	private String fileLocation;

	public DataFromPropertyFile(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String readFromPropertyFile(String keyName) {

		String value = "";
		try {

			file = new File(fileLocation);
			fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			Enumeration enuKeys = properties.keys();
			while (enuKeys.hasMoreElements()) {
				String key = (String) enuKeys.nextElement();
				if (key.equalsIgnoreCase(keyName)) {
					value = properties.getProperty(key);
					// System.out.println(key + ": " + value);
					break;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fileInput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return value;

	}

	public void writeToPropertyFile(String key, String vale) {
		Properties props = new Properties();
		try {
			props.setProperty(key, vale);
			file = new File(fileLocation);
			out = new FileOutputStream(file, true);
			props.store(out, null);
			// props.store(out,"Jira Bug added");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
