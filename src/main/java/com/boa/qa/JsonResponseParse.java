package com.boa.qa;

import com.fasterxml.jackson.core.type.TypeReference;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JsonResponseParse {

	private DataFromPropertyFile fromPropertyFile;
	private String jsonFileName;

	public JsonResponseParse() {

	}

	public JsonResponseParse(String jsonFileName) {
		this.jsonFileName = jsonFileName;

	}

	public String getElementByName(String keyName) {
		String path = System.getProperty("user.dir");
		JSONObject jsonObject = null;
		String jira_id = null;

		try {
			fromPropertyFile = new DataFromPropertyFile(
					path
							+ "\\src\\test\\resources\\Resources\\PropertyFile.property");
			jsonObject = getJSONObject(jsonFileName);

			jira_id = jsonObject.get(keyName).toString();

			/*
			 * if(jira_id!=null){ fromPropertyFile.writeToPropertyFile(keyName,
			 * jira_id); }
			 */
		} catch (Exception e) {

		}
		return jira_id;
	}

	private JSONObject getJSONObject(String fileName) {
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = null;
		try {

			Object obj = parser.parse(new FileReader(fileName));

			jsonObject = (JSONObject) obj;
			System.out.println(jsonObject);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	public String returnElementByName(String elementName) {

		JSONObject jsonObject = null;
		String name = null;
		String key = null;
		try {

			jsonObject = getJSONObject(jsonFileName);
			System.out.println(jsonObject);
			if (!elementName.equalsIgnoreCase("message")
					&& !elementName.equalsIgnoreCase("id")) {
				if (jsonObject.get(elementName).toString().equals("true")) {
					Boolean bool = (Boolean) jsonObject.get(elementName);
					name = String.valueOf(bool);
				}

				else if (jsonObject.get(elementName).toString().equals("false")) {
					Boolean bool = (Boolean) jsonObject.get(elementName);
					name = String.valueOf(bool);
				}

				else if (jsonObject.get(elementName).toString().equals("false")) {
					Boolean bool = (Boolean) jsonObject.get(elementName);
					name = String.valueOf(bool);
				}

				else {
					// name = (String) jsonObject.get(elementName);
					name = String.valueOf(jsonObject.get(elementName));
				}
			}
			if (elementName.equalsIgnoreCase("message")) {
				Map address = ((Map) jsonObject.get("result"));

				// iterating address Map
				Iterator<Map.Entry> itr1 = address.entrySet().iterator();
				while (itr1.hasNext()) {
					Map.Entry pair = itr1.next();
					System.out.println(pair.getKey() + " : " + pair.getValue());
					name = (String) pair.getValue();
				}
			}

			else if (elementName.equalsIgnoreCase("id")) {
				Map address = ((Map) jsonObject.get("result"));

				// iterating address Map
				Iterator<Map.Entry> itr1 = address.entrySet().iterator();
				while (itr1.hasNext()) {
					Map.Entry pair = itr1.next();
					key = (String) pair.getKey();
					if (key.equalsIgnoreCase("id")) {
						System.out.println(pair.getKey() + " : "
								+ pair.getValue());
						name = (String) pair.getValue();
					}
				}
			}

			System.out.println(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
	}

	public JSONArray getJSONArray(String elementName) {
		JSONObject jsonObject = null;
		JSONObject jsonObjectArry = null;
		JSONArray msg = null;
		String name = "";
		try {
			jsonObject = getJSONObject(jsonFileName);
			if (elementName.equalsIgnoreCase("result")) {
				Map address = ((Map) jsonObject.get("result"));
				Iterator<Map.Entry> itr1 = address.entrySet().iterator();
				while (itr1.hasNext()) {
					Map.Entry pair = itr1.next();
					System.out.println(pair.getKey() + " : " + pair.getValue());
					name = (String) pair.getKey();
					if (name.equalsIgnoreCase("cart")) {
						jsonObjectArry = (JSONObject) jsonObject
								.get(elementName);
						msg = (JSONArray) jsonObjectArry.get(name);
					}

					if (name.equalsIgnoreCase("customer")) {
						jsonObjectArry = (JSONObject) jsonObject
								.get(elementName);
						msg = (JSONArray) jsonObjectArry.get(name);
					}

					if (name.equalsIgnoreCase("transactions")) {
						jsonObjectArry = (JSONObject) jsonObject
								.get(elementName);
						msg = (JSONArray) jsonObjectArry.get(name);
					}

					if (name.equalsIgnoreCase("transaction")) {
						jsonObjectArry = (JSONObject) jsonObject
								.get(elementName);
						msg = (JSONArray) jsonObjectArry.get(name);
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return msg;
	}

	public Map<Object, Object> getResponseMessage(String fileName) {
		JSONObject jsonObject = getJSONObject(fileName);
		String jsonStr = jsonObject.toJSONString();
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		ObjectMapper mapperObj = new ObjectMapper();

		// System.out.println("Input Json: "+jsonStr);
		try {
			resultMap = mapperObj.readValue(jsonStr,
					new TypeReference<HashMap<Object, Object>>() {
					});
			// System.out.println("Output Map: "+resultMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}

	public Map<Object, Object> getResponseMessage(JSONObject jObject) {
		JSONObject jsonObject = jObject;
		String jsonStr = jsonObject.toJSONString();
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		ObjectMapper mapperObj = new ObjectMapper();

		System.out.println("Input Json: " + jsonStr);
		try {
			resultMap = mapperObj.readValue(jsonStr,
					new TypeReference<HashMap<Object, Object>>() {
					});
			System.out.println("Output Map: " + resultMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}

	public int countNumberOfFiles(String fileNamePath) {
		int fileCount = 0;
		try {
			File directory = new File(fileNamePath);
			fileCount = directory.list().length;
			System.out.println("File Count:" + fileCount);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return fileCount;
	}

	public JSONObject getJSONObject(String elementName, String jsonFileName) {
		JSONObject jsonObject = null;
		JSONObject jsonObjectArry = null;

		String name = "";
		try {
			jsonObject = getJSONObject(jsonFileName);
			if (elementName.equalsIgnoreCase("result")) {
				Map address = ((Map) jsonObject.get("result"));
				Iterator<Map.Entry> itr1 = address.entrySet().iterator();
				while (itr1.hasNext()) {
					Map.Entry pair = itr1.next();
					System.out.println(pair.getKey() + " : " + pair.getValue());
					name = (String) pair.getKey();
					if (name.equalsIgnoreCase("cart")) {
						jsonObjectArry = (JSONObject) jsonObject
								.get(elementName);
						jsonObject = (JSONObject) jsonObjectArry.get(name);
					}

					if (name.equalsIgnoreCase("customer")) {
						jsonObjectArry = (JSONObject) jsonObject
								.get(elementName);
						jsonObject = (JSONObject) jsonObjectArry.get(name);
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return jsonObject;
	}
}
