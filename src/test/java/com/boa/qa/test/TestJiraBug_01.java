package com.boa.qa.test;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.HttpMethod;

import com.boa.qa.CreateJiraBug;
import com.boa.qa.DataFromPropertyFile;
import com.boa.qa.JsonResponseParse;

public class TestJiraBug_01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String path_ResponseParse_File = "C:\\Users\\PaxoTech Student\\Desktop\\JsonResponse\\response_1.json";
		String path_Response_File = "C:\\Users\\PaxoTech Student\\Desktop\\JsonResponse\\response_1";
		// property file location
		String propertyFileLocation = System.getProperty("user.dir")
				+ "\\src\\test\\resources\\Resources\\PropertyFile.property";
		// create property instance
		DataFromPropertyFile propertyFile = new DataFromPropertyFile(
				propertyFileLocation);
		// url
		String url = propertyFile.readFromPropertyFile("server_name");
		// basepath
		String basePath = propertyFile.readFromPropertyFile("base_path");
		// action
		String action = propertyFile.readFromPropertyFile("action");
		// userName
		String userName = propertyFile.readFromPropertyFile("user_name");
		// password
		String password = propertyFile.readFromPropertyFile("password");
		
		
		// TC defect ID
		String tc_defectID;
		List<String> TC = new ArrayList<String>();
		TC.add("pass");//xcel pass/pass er colum looop korso 
		TC.add("fail");
		TC.add("pass");
		TC.add("fail");
		TC.add("pass");
		TC.add("pass");
		TC.add("fail");
		
		
		int length=0;
		for (int j = 0; j < TC.size(); j++) {
			
			if (TC.get(j).toString().equals("fail")) {
				//testcase value from excel false hole fail hoise
				tc_defectID = propertyFile.readFromPropertyFile("TC_Number"+ j);//my own keyof testcaseStus in property filr to store the value of this key from xcel//checking precondition if faieldand defect already created
				length=	tc_defectID.length();//String er lenth janar jonno	
				
				System.out.println(tc_defectID.length());//for debugging to see the length  in now
				if (length==0) //if 0 then it was not created as a defect before

					{
					
					
					CreateJiraBug jiraBug = new CreateJiraBug();
					jiraBug.connect(url + basePath + action, userName,
							password, HttpMethod.POST, path_Response_File);// save response in this path as formated

					JsonResponseParse responseParse = new JsonResponseParse(
							path_ResponseParse_File);// doing parse

					String id = responseParse.getElementByName("id");// got key(ID) and based on ID got value from response
					propertyFile.writeToPropertyFile("TC_Number" + j, id);// save this key( id) value back to  in property file
				
				} else {
					
					System.out.println("defectID is already:" + tc_defectID);
				}

			}
			else
			{
			System.out.println("Test case is passed:"+	TC.get(j).toString());
			}

		}

	}
}
